package game

import (
	"sync"
)

// Direction (Top, Bottom, Left, Right)
type Direction int

const (
	Up Direction = iota
	Down
	Left
	Right
)

// String representation of the Direction
func (d Direction) String() string {
	switch d {
	case Up:
		return "Up"
	case Down:
		return "Down"
	case Left:
		return "Left"
	case Right:
		return "Right"
	default:
		return ""
	}
}

// Position represents by the (x,y) coordinates
type Position struct {
	X uint
	Y uint
}

// IsEquel checks if two Positions are same
func (a Position) IsEquel(b Position) bool {
	if a.X == b.X && a.Y == b.Y {
		return true
	}
	return false
}

// PositionQueue : Implementing a queue of Positions
type PositionQueue struct {
	Positions []Position
	lock      sync.RWMutex
}

// New creates a new PositionQueue
func (p *PositionQueue) New() *PositionQueue {
	p.Positions = []Position{}
	return p
}

// PushFront adds a Position to the front of the queue
func (p *PositionQueue) PushFront(t Position) {
	p.lock.Lock()
	p.Positions = append([]Position{t}, p.Positions...)
	p.lock.Unlock()
}

// PopLast removes a Position from the end of the queue
func (p *PositionQueue) PopLast() {
	p.lock.Lock()
	p.Positions = p.Positions[:len(p.Positions)-1]
	p.lock.Unlock()
}

// Head returns the Position on the front of the queue
func (p *PositionQueue) Head() *Position {
	p.lock.RLock()
	Position := p.Positions[0]
	p.lock.RUnlock()
	return &Position
}

// IsEmpty returns true if the queue is empty
func (p *PositionQueue) IsEmpty() bool {
	return len(p.Positions) == 0
}

// Size returns the number of Positions in the queue
func (p *PositionQueue) Size() int {
	return len(p.Positions)
}

// Contains returns if a item is in the queue
func (p *PositionQueue) Contains(item Position) bool {
	for _, x := range p.Positions {
		if x.IsEquel(item) {
			return true
		}
	}
	return false
}
