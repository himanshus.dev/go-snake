package game

import (
	"sync"

	"gitlab.com/himanshus.dev/go-snake/pkg/utils"
)

// SnakeGame - Structure for Game State
type SnakeGame struct {
	Width     uint
	Height    uint
	Snake     PositionQueue
	Direction Direction
	Food      Position
	Lost      bool
	Won       bool
	Changed   bool
	lock      sync.RWMutex
}

// New creates a new SnakeGame
func (s *SnakeGame) New(width uint, height uint) *SnakeGame {
	s = &SnakeGame{
		Width:     width,
		Height:    height,
		Snake:     PositionQueue{Positions: []Position{{utils.Max(width-3, 0), height / 2}}},
		Food:      Position{utils.Min(width/2, 2), height / 2},
		Direction: Left,
		Lost:      false,
		Won:       false,
		Changed:   false,
	}
	return s
}

// ChangeDirection sets the Direction snake is moving as per user input
func (s *SnakeGame) ChangeDirection(d Direction) *SnakeGame {
	if s.IsOver() {
		return s
	}
	// Do nothiing for same or opposit Direction
	if s.Direction == Up && d == Up ||
		s.Direction == Up && d == Down ||
		s.Direction == Down && d == Down ||
		s.Direction == Down && d == Up ||
		s.Direction == Left && d == Left ||
		s.Direction == Left && d == Right ||
		s.Direction == Right && d == Right ||
		s.Direction == Right && d == Left {
		return s
	}
	s.lock.Lock()
	s.Direction = d
	s.lock.Unlock()
	return s
}

// Tick updates the Position of Snake on the board
func (s *SnakeGame) Tick() {
	// Move Snake if Possible
	if s.Lost || s.Snake.IsEmpty() {
		return
	}
	// Calculate new_head
	var head = s.Snake.Head()
	var new_head Position
	switch s.Direction {
	case Up:
		new_head = Position{head.X - 1, head.Y}
	case Right:
		new_head = Position{head.X, head.Y + 1}
	case Down:
		new_head = Position{head.X + 1, head.Y}
	case Left:
		new_head = Position{head.X, head.Y - 1}
	}
	// Check collision with board
	if new_head.X > s.Width || new_head.Y > s.Height {
		s.lock.Lock()
		s.Lost = true
		s.lock.Unlock()
		return
	}
	// Move to new_head
	s.Snake.PushFront(new_head)
	// Remove the last element if not eating food
	if !s.Food.IsEquel(new_head) && s.Snake.Size() > 1 {
		s.Snake.PopLast()
	} else {
		s.RespawnFood()
	}
	// Check collision with body
	// this implementation will get slower as the size of snake increases
	if s.Snake.Size() >= 4 {
		for _, pos := range s.Snake.Positions[2:] {
			if pos.IsEquel(new_head) {
				s.lock.Lock()
				s.Lost = true
				s.lock.Unlock()
			}
		}
	}
	s.lock.Lock()
	s.Changed = true
	s.lock.Unlock()

}

// RespawnFood generate a new random position for food
func (s *SnakeGame) RespawnFood() *SnakeGame {
	var new_food Position
	// Not a good idea, probably we can do something else... IDK
	for {
		// If the snake is already covering the whole board, don't worry about food
		if s.Snake.Size() == int(s.Width*s.Height) {
			s.lock.Lock()
			s.Won = true
			s.lock.Unlock()
			return s
		}
		new_food = Position{X: utils.Random(s.Width), Y: utils.Random(s.Height)}
		// Can't render food at the same location
		if s.Food.IsEquel(new_food) {
			continue
		}
		// Can't render food on snake's body
		for _, pos := range s.Snake.Positions {
			// We've found a good location to spawn food
			if !pos.IsEquel(new_food) {
				s.lock.Lock()
				s.Food = new_food
				s.lock.Unlock()
				return s
			}
		}
	}
}

// IsOver returns true if user have won or lost
func (s *SnakeGame) IsOver() bool {
	if s.Lost || s.Won {
		return true
	}
	return false
}

// ClearChanged resets the UI changed flag
func (s *SnakeGame) ClearChanged() {
	s.lock.Lock()
	s.Changed = false
	s.lock.Unlock()
}
