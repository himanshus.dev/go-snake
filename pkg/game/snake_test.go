package game

import (
	"testing"
)

// TestGame checks the basic functionality of the Game
func TestGame(t *testing.T) {
	t.Run("state_test", func(t *testing.T) {
		var game *SnakeGame
		game = game.New(20, 20)
		var x = 0
		for !game.IsOver() {
			t.Log("Turn:", x+1)
			t.Logf("%#v\n", game.Snake.Positions)
			game.Tick()
			x++
		}
		t.Logf("%#v\n", game)
	})
}
