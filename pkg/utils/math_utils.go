package utils

import "math/rand"

func Max(x, y uint) uint {
	if x > y {
		return x
	}
	return y
}

func Min(x, y uint) uint {
	if x < y {
		return x
	}
	return y
}

func Random(max uint) uint {
	return uint(rand.Intn(int(max)))
}
