# Go-Snake

An attempt at making a basic Snake game with Go and WASM and learn how to use WebAssembly with Go in the process.

## Prerequisite
 - [Go](https://go.dev/dl/)
 - [Make](https://www.gnu.org/software/make/#download)

## Running the code
The project uses `make` for building the code. You can use the following command to build the game:

```bash
git clone https://gitlab.com/himanshus.dev/go-snake.git
cd go-snake && make
```
You have to rebuild the WASAM binary everytime you make a chnage in the [game](/cmd/wasm//main.go). You can use the `run` command to quickly do so:

```bash
make run
```


## Tasks
#### Must
 - [x] Game state management.
 - [x] Renering the game with WASM.
 - [x] Event handling for game input.
 - [x] Create a makefile.

#### Maybe
 - [ ] Add screenshots or gif for the game.
 - [ ] Setup a UI to configure game parameters, like:
        - Choose speed.
        - Choose snake avatar.
        - Choose food avatar.
        - Play button.



## Thanks
Thanks to [Yichuan Shen](https://github.com/yishn) for the idea. I saw his `Rust + WASM` youtube [video](https://youtu.be/iR7Q_6quwSI) and wanted to follow along. But to add some challenge, I created the game in `Go + WASM` instead. 