package main

import (
	"fmt"
	"reflect"
	"syscall/js"

	snake "gitlab.com/himanshus.dev/go-snake/pkg/game"
)

var game *snake.SnakeGame
var intervalID js.Value

func main() {
	// Initialize the game
	game = game.New(20, 20)
	// Render Initial UI
	renderGameUI()
	// Add event listner
	js.Global().Get("document").Call("addEventListener", "keypress", js.FuncOf(keypressCallBack))
	// Add Game-Loop
	intervalID = js.Global().Call("setInterval", js.FuncOf(updateGameUI), "500")

	// Empty select block will cause main to run foreever
	select {}
}

func renderGameUI() {
	// Get the #root div
	var root = js.Global().Get("document").Call("getElementById", "root")
	// Set style of grid accoding to game board size
	root.Set("style", fmt.Sprintf("grid-template: repeat(%d, auto) / repeat(%d, auto)", game.Width, game.Height))

	// If alrady over - Do nothing
	if game.IsOver() {
		return
	}
	fmt.Println("Painting Canvas")
	// Clean the root div enery time
	root.Set("innerHTML", "")
	// Draw the game
	var row, col uint
	for row = 0; row < game.Width; row++ {
		for col = 0; col < game.Height; col++ {
			// render food
			if game.Food.IsEquel(snake.Position{X: row, Y: col}) {
				rootContent := root.Get("innerHTML").String()
				root.Set("innerHTML", fmt.Sprintf("%s%s", rootContent, "<div class=\"box\">🎯</div>"))
				continue
			}
			// render snake
			if game.Snake.Contains(snake.Position{X: row, Y: col}) {
				rootContent := root.Get("innerHTML").String()
				if game.Snake.Positions[0].IsEquel(snake.Position{X: row, Y: col}) {
					root.Set("innerHTML", fmt.Sprintf("%s%s", rootContent, "<div class=\"box\">✴️</div>"))
				} else {
					root.Set("innerHTML", fmt.Sprintf("%s%s", rootContent, "<div class=\"box\">🟨</div>"))
				}
			} else {
				rootContent := root.Get("innerHTML").String()
				root.Set("innerHTML", fmt.Sprintf("%s%s", rootContent, "<div class=\"box\"></div>"))
			}
		}
	}
}

func keypressCallBack(this js.Value, args []js.Value) interface{} {
	event := args[0]
	var keycode string
	keycode = event.Get("code").String()
	fmt.Println("Key Pressed:", keycode)
	if !(reflect.TypeOf(keycode) == reflect.TypeOf("")) {
		keycode = ""
	}
	if keycode == "KeyW" {
		game = game.ChangeDirection(snake.Up)
	} else if keycode == "KeyS" {
		game = game.ChangeDirection(snake.Down)
	} else if keycode == "KeyD" {
		game = game.ChangeDirection(snake.Right)
	} else if keycode == "KeyA" {
		game = game.ChangeDirection(snake.Left)
	}
	fmt.Println("New Direction:", game.Direction.String())
	return nil
}

func updateGameUI(this js.Value, args []js.Value) interface{} {
	// fmt.Printf("Direction: %#v\n", game.Direction.String())

	// Update Score
	var state = js.Global().Get("document").Call("getElementById", "state")
	var score = js.Global().Get("document").Call("getElementById", "score")

	// If GameOver Remove Interval
	if game.IsOver() {
		state.Set("innerHTML", fmt.Sprint("<strong>State:</strong> Game Over"))
		score.Set("innerHTML", fmt.Sprintf("<strong>Score:</strong> %d", game.Snake.Size()))
		js.Global().Call("clearInterval", intervalID)
		fmt.Printf("Sate: %#v\n", game)
		return nil
	}
	// Make a move
	game.Tick()
	if !game.Changed {
		return nil
	}
	renderGameUI()
	game.ClearChanged()
	state.Set("innerHTML", fmt.Sprint("<strong>State:</strong> Playing"))
	score.Set("innerHTML", fmt.Sprintf("<strong>Score:</strong> %d", game.Snake.Size()))
	return nil
}
