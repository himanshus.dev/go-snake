package main

import (
	"flag"
	"fmt"
	"io/fs"
	"net/http"

	"gitlab.com/himanshus.dev/go-snake/pkg/dist"
)

func main() {
	address := flag.String("addr", "127.0.0.1", "Web Server Address")
	port := flag.String("port", "9000", "Web Server Port")
	flag.Parse()

	// Init a static file server
	fsys := fs.FS(dist.DistFiles)
	fileServer := http.FileServer(http.FS(fsys))

	// Create a server mux
	mux := http.NewServeMux()
	mux.Handle("/", fileServer)

	// Run the server
	fmt.Printf("Server runnig at: http://%s:%s \n", *address, *port)
	panic(http.ListenAndServe(fmt.Sprintf("%s:%s", *address, *port), mux))
}
