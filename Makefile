GAME_BIN=snake
WASM_BIN=./pkg/dist/snake.wasm
SOURCE := $(shell find . -name "*.go")
SOURCE += $(shell find . -name "*.js")
SOURCE += $(shell find . -name "*.html")
SOURCE += $(shell find . -name "*.wasm")

DETECTED_OS=linux

ifeq ($(OS),Windows_NT) 
    DETECTED_OS=windows
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Darwin)
        DETECTED_OS=darwin
	else
    	DETECTED_OS=linux
    endif
endif

all: wasm game

tidy:
	go mod tidy -v

fmt:
	gofmt -s -w .

run: all
	./$(GAME_BIN)

game: $(SOURCE) Makefile
	GOOS=$(DETECTED_OS) GOARCH=amd64 go build -ldflags="-s -w" -o $(GAME_BIN) ./cmd/snake

wasm: $(SOURCE) Makefile
	GOOS=js GOARCH=wasm go build -ldflags="-s -w" -o $(WASM_BIN) ./cmd/wasm

clean:
	rm -rf $(WASM_BIN)
	rm -rf $(GAME_BIN)

.PHONY: all tidy fmt wasm game clean